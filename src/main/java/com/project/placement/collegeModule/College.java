package com.project.placement.collegeModule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "college")
public class College {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "admin")
    private String collegeAdmin;
    @Column(name = "name")
    private String collegeName;
    @Column(name = "location")
    private String location;
    
    
    public College() {
        super();
    }
    public College(Integer id, String collegeAdmin, String collegeName, String location) {
        this.id = id;
        this.collegeAdmin = collegeAdmin;
        this.collegeName = collegeName;
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCollegeAdmin() {
        return collegeAdmin;
    }

    public void setCollegeAdmin(String collegeAdmin) {
        this.collegeAdmin = collegeAdmin;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "College [id=" + id + ", collegeAdmin=" + collegeAdmin + ", collegeName=" + collegeName + ", location="
                + location + ", getId()=" + getId() + ", getCollegeAdmin()=" + getCollegeAdmin() + ", getCollegeName()="
                + getCollegeName() + ", getLocation()=" + getLocation() + ", getClass()=" + getClass() + ", hashCode()="
                + hashCode() + ", toString()=" + super.toString() + "]";
    }    
}
