package com.project.placement.studentModule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "college")
    private String college;
    @Column(name = "roll")
    private long roll;
    @Column(name = "qualification")
    private String qualification;
    @Column(name = "year")
    private int year; 
    @Column(name = "certificate")
    private long certificate;
    
    public Student() {
        super();
    }

    public Student(Integer id, String name, String college, long roll, String qualification,
            int year, long certificate) {
        this.id = id;
        this.name = name;
        this.college = college;
        this.roll = roll;
        this.qualification = qualification;
        this.year = year;
        this.certificate = certificate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public long getRoll() {
        return roll;
    }

    public void setRoll(long roll) {
        this.roll = roll;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getCertificate() {
        return certificate;
    }

    public void setCertificate(long certificate) {
        this.certificate = certificate;
    }

    @Override
    public String toString() {
        return "StudentService [id=" + id + ", name=" + name + ", college=" + college + ", roll=" 
                + roll  + ", qualification=" + qualification + ", year=" + year + ", certificate=" 
                + certificate + ", getId()=" + getId() + ", getName()=" + getName() + ", getCollege()=" 
                + getCollege() + ", getRoll()=" + getRoll() + ", getQualification()=" + getQualification() 
                + ", getYear()=" + getYear() + ", getCertificate()=" + getCertificate() + ", getClass()=" 
                + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
    }
}
