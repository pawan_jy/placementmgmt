package com.project.placement.certificateModule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "certificate")
public class Certificate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "year")
    private int year;
    @Column(name = "college")
    private String college;
    
    public Certificate() {
        super();
    }
    public Certificate(Integer id, int year, String college) {
        super();
        this.id = id;
        this.year = year;
        this.college = college;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    @Override
    public String toString() {
        return "Certificate [id=" + id + ", year=" + year + ", college=" + college + ", getId()=" + getId()
                + ", getYear()=" + getYear() + ", getCollege()=" + getCollege() + ", getClass()=" + getClass()
                + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
    }
}
