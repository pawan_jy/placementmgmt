package com.project.placement.placementModule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "placement")
public class Placement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "college")
    private String college;
    @Column(name = "qualification")
    private String qualification;
    @Column(name = "year")
    private Integer year;

    public Placement() {
        super();
    }
    public Placement(Integer id, String name, String college, String qualification, Integer year) {
        this.id = id;
        this.name = name;
        this.college = college;
        this.qualification = qualification;
        this.year = year;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Placement [id=" + id + ", name=" + name + ", college=" + college + ", qualification="
                + qualification + ", year=" + year + ", getId()=" + getId() + ", getName()=" + getName()
                + ", getCollege()="
                + getCollege() + ", getQualification()=" + getQualification()
                + ", getYear()="
                + getYear() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + "]";
    }

}
