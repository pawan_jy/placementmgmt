package com.project.placement.companyModule;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "companies")
public class Company {

    // Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "location")
    private String location;
    @Column(name = "sector")
    private String sector;

    // Constructors
    public Company(){
        super();
    }
    public Company(Integer id, String name, String location, String sector) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.sector = sector;
    }

    // Getters and Setters
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getSector() {
        return sector;
    }
    public void setSector(String sector) {
        this.sector = sector;
    }

    // Overridden toString Method
    @Override
    public String toString() {
        return "Company [id=" + id + ", name=" + name + ", location=" + location + ", sector=" + sector + "]";
    }
}
