package com.project.placement.companyModule;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyController {
    @Autowired
    private CompanyService compService;

    // RESTful API methods for Retrieval operations
    @GetMapping("/companies")
    public List<Company> list() {
        return compService.listAll();
    }

    @GetMapping("/companies/{id}")
    public ResponseEntity<Company> get(@PathVariable Integer id) {
        try {
            Company company = compService.get(id);
            return new ResponseEntity<Company>(company, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Company>(HttpStatus.NOT_FOUND);
        }
    }

    // RESTful API method for Create operation
    @PostMapping("/companies")
    public void add(@RequestBody Company company) {
        compService.save(company);
    }

    // RESTful API method for Update operation
    @PutMapping("/companies/{id}")
    public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody Company company) {
        try {
            Company exsitedCompany = compService.get(id);
            return new ResponseEntity<Company>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Company>(HttpStatus.NOT_FOUND);
        }
    }

    // RESTful API method for Delete operation
    @DeleteMapping("/companies/{id}")
    public void delete(@PathVariable Integer id) {
        compService.delete(id);
    }
}
