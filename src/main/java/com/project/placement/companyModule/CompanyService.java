package com.project.placement.companyModule;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CompanyService {
    @Autowired
    private CompanyRepository compRepo;

    public List<Company> listAll() {
        return compRepo.findAll();
    }

    public void save(Company company) {
        compRepo.save(company);
    }

    public Company get(Integer id) {
        return compRepo.findById(id).get();
    }

    public void delete(Integer id) {
        compRepo.deleteById(id);
    }
}