## Read Operation (GET Method)
Following API Request will show all the entities

`curl --location --request GET '127.0.0.1:8080/companies'`


## Create Operation (POST Method)
Following API Request will create a new entry in database with attcahed fields of the JSON

`curl --location --request POST '127.0.0.1:8080/companies/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": 104,
    "name": "TCS",
    "location": "Noida",
    "sector": "Sales"
}'`

## Update Operation (PUT Method)
Following API Request will modify an existing entry in database

`curl --location --request PUT '127.0.0.1:8080/companies/105' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "TCS",
    "location": "Noida",
    "sector": "IT"
}'`

## Delete Operation (DELETE Method)
This will delete an entry

`curl --location --request DELETE '127.0.0.1:8080/companies/105'`