CREATE SCHEMA `placement` ;

CREATE TABLE `placement`.`companies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NULL,
  `sector` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

INSERT INTO `placement`.`companies` (`id`, `name`, `location`, `sector`) VALUES ('100', 'Capgemini', 'Bangalore', 'IT');
INSERT INTO `placement`.`companies` (`id`, `name`, `location`, `sector`) VALUES ('101', 'IBM', 'Mumbai', 'Sales');
INSERT INTO `placement`.`companies` (`id`, `name`, `location`, `sector`) VALUES ('102', 'Google', 'Pune', 'Management');
INSERT INTO `placement`.`companies` (`id`, `name`, `location`, `sector`) VALUES ('103', 'Technoserve', 'Chennai', 'IT');




CREATE TABLE `placement`.`certificate` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `year` INT NOT NULL,
  `college` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

INSERT INTO `placement`.`certificate` (`id`, `year`, `college`) VALUES ('100', '2019', 'Hindu College');
INSERT INTO `placement`.`certificate` (`id`, `year`, `college`) VALUES ('101', '2018', 'St. Stephen College');
INSERT INTO `placement`.`certificate` (`id`, `year`, `college`) VALUES ('102', '2019', 'Madras Christian College');




CREATE TABLE `placement`.`admin` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `pass` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE);

INSERT INTO `placement`.`admin` (`id`, `name`, `pass`, `salt`) VALUES ('1', 'user1', 'pass1', 'salt1');
INSERT INTO `placement`.`admin` (`id`, `name`, `pass`, `salt`) VALUES ('2', 'user2', 'pass123', 'salt123');




CREATE TABLE `placement`.`college` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NULL,
  `admin` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

INSERT INTO `placement`.`college` (`id`, `name`, `location`, `admin`) VALUES ('100', 'Hindu College', 'Delhi', 'user01');
INSERT INTO `placement`.`college` (`id`, `name`, `location`, `admin`) VALUES ('101', 'St. Stephen College', 'Bangalore', 'user06');
INSERT INTO `placement`.`college` (`id`, `name`, `location`, `admin`) VALUES ('102', 'Madras Christian College', 'Chennai', 'user11');




CREATE TABLE `placement`.`placement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `college` VARCHAR(45) NOT NULL,
  `qualification` VARCHAR(45) NOT NULL,
  `year` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

INSERT INTO `placement`.`placement` (`id`, `name`, `college`, `qualification`, `year`) VALUES ('1001', 'Avinash', 'Hindu College', 'B.Tech.', '2020');
INSERT INTO `placement`.`placement` (`id`, `name`, `college`, `qualification`, `year`) VALUES ('1002', 'Manisha', 'St. Stephen College', 'M.Tech', '2022');
INSERT INTO `placement`.`placement` (`id`, `name`, `college`, `qualification`, `year`) VALUES ('1003', 'Paban', 'Madras Christian College', 'B.Sc.', '2018');




CREATE TABLE `placement`.`student` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `college` VARCHAR(45) NOT NULL,
  `roll` VARCHAR(45) NOT NULL,
  `qualification` VARCHAR(45) NOT NULL,
  `year` VARCHAR(45) NOT NULL,
  `certificate` BIGINT(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

INSERT INTO `placement`.`student` (`id`, `name`, `college`, `roll`, `qualification`, `year`, `certificate`) VALUES ('101', 'Akhil', 'Hindu College', '0265', 'B.Tech.', '2019', '12');
INSERT INTO `placement`.`student` (`id`, `name`, `college`, `roll`, `qualification`, `year`, `certificate`) VALUES ('102', 'Avinash', 'Hindu College', '0126', 'B.Tech.', '2020', '14');
INSERT INTO `placement`.`student` (`id`, `name`, `college`, `roll`, `qualification`, `year`, `certificate`) VALUES ('103', 'Manisha', 'St. Stephen College', '2506', 'M.Tech', '2022', '11');
INSERT INTO `placement`.`student` (`id`, `name`, `college`, `roll`, `qualification`, `year`, `certificate`) VALUES ('104', 'Paban', 'Madras Christian College', '0369', 'B.Sc.', '2018', '22');
INSERT INTO `placement`.`student` (`id`, `name`, `college`, `roll`, `qualification`, `year`, `certificate`) VALUES ('105', 'Ravi', 'St. Stephen College', '2315', 'B.Tech.', '2018', '14');
INSERT INTO `placement`.`student` (`id`, `name`, `college`, `roll`, `qualification`, `year`, `certificate`) VALUES ('106', 'Tarun', 'St. Stephen College', '1506', 'B.Arch.', '2020', '78');
INSERT INTO `placement`.`student` (`id`, `name`, `college`, `roll`, `qualification`, `year`, `certificate`) VALUES ('107', 'Veer', 'Madras Christian College', '0142', 'B.Des.', '2019', '96');




CREATE TABLE `placement`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `pass` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

INSERT INTO `placement`.`user` (`id`, `name`, `type`, `pass`, `salt`) VALUES ('1', 'user001', 'admin', 'pass123', 'salt001');
INSERT INTO `placement`.`user` (`id`, `name`, `type`, `pass`, `salt`) VALUES ('2', 'user002', 'guest', 'pass789', 'salt002');
